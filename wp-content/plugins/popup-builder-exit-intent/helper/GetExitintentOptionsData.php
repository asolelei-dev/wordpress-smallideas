<?php
class GetExitintentOptionsData {

	public static function getOptionsDefaultData() {

		$optionsData = array();

		$radiobuttons = array(
			array(
				"title" => "Soft mode:",
				"value" => "soft",
				"info" => "<span class=\"dashicons dashicons-info repositionImg sameImageStyle\"></span>
							<span class='infoReposition samefontStyle'>
								If the user navigates away from the site the popup will appear.
							</span>"
			),
			array(
				"title" => "Aggressive mode:",
				"value" => "aggressive",
				"info" => "<span class=\"dashicons dashicons-info repositionImg sameImageStyle\"></span>
							<span class='infoReposition samefontStyle'>
								If the user tries to navigate elsewhere he/she will be interrupted and forced to read the message and choose to leave or stay. After the alert box, the popup will appear.
							</span>"
			),
			array(
				"title" => "Soft and Aggressive modes:",
				"value" => "softAndAggressive",
				"info" => "<span class=\"dashicons dashicons-info repositionImg sameImageStyle\"></span>
							<span class='infoReposition samefontStyle'>
								This will enable both of the modes. Depends on which action will be triggered first.
							</span>"
			),
			array(
				"title" => "Aggressive without popup:",
				"value" => "aggressiveWithoutPopup",
				"info" => "<span class='dashicons dashicons-info repositionImg sameImageStyle'></span>
							<span class='infoReposition samefontStyle'>
								This is the same as the Aggressive mode, but without a popup.
							</span>"
			),

		);

		$optionsData['radiobuttons'] = $radiobuttons;

		return $optionsData;
	}

	public function sgBoolToChecked($var) {
		return ($var?'checked':'');
	}

}