<?PHP
$optionsData = GetExitintentOptionsData::getOptionsDefaultData();
$radiobuttons = $optionsData['radiobuttons'];
$extensionManagerObj = new SGPBExtensionManager();
$extensionKey = SGPBExitintentExtension::SGPB_EXTENSION_KEY;
$extensionManagerObj->setExtensionData(@$_GET['id'], $extensionKey);

$sgExitIntentType = $extensionManagerObj->getOptionValue('option-exit-intent-type');
$sgExitIntentExpireTime = $extensionManagerObj->getOptionValue('option-exit-intent-expire-time');
$sgExitIntentAlert = $extensionManagerObj->getOptionValue('option-exit-intent-alert');
$sgExitIntentEnable = $extensionManagerObj->getOptionValue('option-exit-intent-enable', true);
$sgExitIntentCookieLevel = $extensionManagerObj->getOptionValue('option-exit-intent-cookie-level', true);
$optionExitIntentSoftFromTop = $extensionManagerObj->getOptionValue('option-exit-intent-soft-from-top', true);
?>
<div id="special-options">
	<div id="post-body" class="metabox-holder columns-2">
		<div id="postbox-container-2" class="postbox-container">
			<div id="normal-sortables" class="meta-box-sortables ui-sortable">
				<div class="postbox popup-builder-special-postbox">
					<div class="handlediv js-special-title" title="Click to toggle"><br></div>
					<h3 class="hndle ui-sortable-handle js-special-title">
						<span>Exit intent option</span>
					</h3>
					<div class="special-options-content">
						<span class="liquid-width">Enable:</span><input type="checkbox" name="option-exit-intent-enable" <?php echo @$sgExitIntentEnable;?>><br>
						<span><b>Mode</b></span>
                        <div class="sg-sub-option-wrapper">
	                        <?php echo createRadiobuttons(@$radiobuttons, "option-exit-intent-type", true, esc_html($sgExitIntentType), "liquid-width"); ?>
                        </div>
                        <div class="sg-sub-option-wrapper">
                            <span class="liquid-width">Show Popup:</span>
                            <input type="number" class="sg-small-liquid-width" name="option-exit-intent-expire-time" value="<?php echo esc_html($sgExitIntentExpireTime); ?>">
                            <span class='dashicons dashicons-info repositionImg same-image-style'></span>
                            <span class='infoReposition samefontStyle'>
                                If the unit is set "0" the popup will be shown during the session.<br>
                                If the unit is set "-1" the popup will appear always.<br>
                                If the unit is set X the popup will show up after every X count of days.<br>
                            </span>
                        </div>
                        <div class="sg-sub-option-wrapper">
                            <span class="liquid-width">Page level cookie saving</span>
                            <input type="checkbox" name="option-exit-intent-cookie-level" <?php echo $sgExitIntentCookieLevel;?>>
                            <span class="dashicons dashicons-info scrollingImg same-image-style"></span>
                            <span class="info-active-url samefontStyle"> If this option is checked popup's cookie will be saved for a current page. By default cookie is set for all site.</span>
                        </div>
						<div class="sg-sub-option-wrapper">
							<span class="liquid-width">Detect exit only from top bar:</span>
							<input type="checkbox" name="option-exit-intent-soft-from-top" <?php echo $optionExitIntentSoftFromTop;?>>
						</div>
                        <div class="sg-sub-option-wrapper">
                            <span class="liquid-width">Alert text:</span>
                            <input  class="input-width-static" type="text" name="option-exit-intent-alert" value="<?php echo esc_attr(@$sgExitIntentAlert); ?>">
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>