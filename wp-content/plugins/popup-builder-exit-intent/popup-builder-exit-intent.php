<?php
/**
 * Plugin Name: Popup Builder Exit intent
 * Plugin URI: http://popup-builder.com/
 * Description: Integrate Exit Intent extension into the Popup Builder.
 * Version:	1.0.2
 * Author: Sygnoos
 * Author URI: http://popup-builder.com/
 * License:
 */

require_once(dirname(__FILE__)."/config/config.php");
require_once(SGPB_EXIT_INTENT_CLASSES."SGEXMain.php");

$sgpbExit = new SGEXMain();
