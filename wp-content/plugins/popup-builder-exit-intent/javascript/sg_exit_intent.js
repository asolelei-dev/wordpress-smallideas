function SGExitIntentPopup() {
	this.exitIntntType;
	this.expireTime;
	this.sgPopupObj = new SGPopup();
	this.alertText;
	this.cookiePageLevel;
	this.exitSoftFromTop;
	this.formLinkClick = false;
}

SGExitIntentPopup.prototype.setType = function(type) {
	this.exitIntntType = type;
};

SGExitIntentPopup.prototype.getType = function() {
	return this.exitIntntType;
};

SGExitIntentPopup.prototype.setExpireTime = function(time) {
	this.expireTime = time;
};

SGExitIntentPopup.prototype.getExpireTime = function() {
	return this.expireTime;
};

SGExitIntentPopup.prototype.setAlertText = function(text) {
	this.alertText = text;
};

SGExitIntentPopup.prototype.getAlertText = function() {
	return this.alertText;
};

SGExitIntentPopup.prototype.setCookiePageLevel = function(cookieLevel) {
	var popupObj = this.sgPopupObj;
	this.cookiePageLevel = popupObj.varToBool(cookieLevel);
};

SGExitIntentPopup.prototype.getCookiePageLevel = function() {
	return this.cookiePageLevel;
};

SGExitIntentPopup.prototype.setExitSoftFromTop = function(exitSoftFromTop) {
	var popupObj = this.sgPopupObj;
	this.exitSoftFromTop = popupObj.varToBool(exitSoftFromTop);
};

SGExitIntentPopup.prototype.getExitSoftFromTop = function() {
	return this.exitSoftFromTop;
};

SGExitIntentPopup.prototype.buildExitIntent = function(id) {
	var type = this.getType();
	var that = this;

	this.linkClickListener();
	
	if(type == "soft") {
		this.softMode(id);
	}
	else if(type == "aggressive") {
		this.aggressiveMode(id);
	}
	else if(type == "softAndAggressive") {
		this.softAndAggressiveMode(id);
	}
	else if(type == "aggressiveWithoutPopup") {
		this.aggressiveMode(id);
	}
};

SGExitIntentPopup.prototype.softMode = function(id) {
	var that = this;
	var leaveFromTop = this.getExitSoftFromTop();

	sgAddEvent(document, 'mouseout',function(e) {
		if (e.toElement == null && e.relatedTarget == null) {
			var result = that.canOpen(id, 'soft');
			if (result) {
				return;
			}

			if (!leaveFromTop) {
				if (jQuery("#sgcolorbox").css("display") !== "block") { /* Check colorbox is open */
					that.sgPopupObj.showPopup(id, false);
				}
			}
			var e = e ? e : window.event;

			/*If this is an autocomplete element.*/
			if (e.target.tagName.toLowerCase() == "input") {
				return;
			}

			/*Get the current viewport width.*/
			var vpWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

			/*If the current mouse X position is within 50px of the right edge*/
			/*of the viewport, return.*/
			if (e.clientX >= (vpWidth - 50)) {
				return;
			}

			/*If the current mouse Y position is not within 50px of the top*/
			/*edge of the viewport, return.*/
			if (e.clientY >= 50)
				return;

			/*Reliable, works on mouse exiting window and*/
			/*user switching active program*/
			var from = e.relatedTarget || e.toElement;
			if (!from && leaveFromTop) {
				if (jQuery("#sgcolorbox").css("display") !== "block") { /* Check colorbox is open */
					that.sgPopupObj.showPopup(id, false);
				}
				return;
			}
		}

	});
};

SGExitIntentPopup.prototype.linkClickListener = function () {

	var that = this;
	var atags = document.getElementsByTagName('a');
	if(atags) {
		for(var tag in atags) {
			sgAddEvent(atags[tag], "click", function (e) {
				that.formLinkClick = true;
			});
		}
	}

};

SGExitIntentPopup.prototype.aggressiveMode = function(id) {
	var that = this;
	sgAddEvent(window, "beforeunload", function (e) {
		var result = that.canOpen(id, 'aggressive');
		if(result || that.formLinkClick){
			return;
		}
		(e || window.event).returnValue = that.triggerOpenPopup(id);
		e.returnValue = that.triggerOpenPopup(id);
	});
};

SGExitIntentPopup.prototype.softAndAggressiveMode = function(id) {
	this.softMode(id);
	this.aggressiveMode(id);
};

SGExitIntentPopup.prototype.triggerOpenPopup = function(id) {
	if(this.getType() !== 'aggressiveWithoutPopup') {
		this.sgPopupObj.showPopup(id, false);
	}
	return this.getAlertText();
};

SGExitIntentPopup.prototype.canOpen = function(id, type) {

	if(!jQuery.cookie('SGExitIntentPopup'+id+type)) {
		this.setCookies(id, type);
		return false;
	}
	return true;
};

SGExitIntentPopup.prototype.setCookies = function(id, type) {
	var that = this;
	var date = this.getExpireTime();
	var pageLevel = true;
	date = parseInt(date);

	jQuery(document).ready(function() {
		jQuery('#sgcolorbox').on('sgPopupClose', function(e) {

			if(that.getCookiePageLevel() == true) {
                pageLevel = false;
			}
			/*Date == -1 for always case*/
			if(date !== -1) {
				if(date == 0) {
					/*Date == 0 for session case*/
                    SGPopup.setCookie('SGExitIntentPopup'+id+type,id, -1, pageLevel);
                    return true;
				}
                SGPopup.setCookie('SGExitIntentPopup'+id+type,id, date, pageLevel);
				return true;
			}
		});
	});
};

SGExitIntentPopup.prototype.init = function(id) {
	if(typeof SG_POPUP_DATA[id] == 'undefined') {
		return;
	}
	var data = SG_POPUP_DATA[id];
	var exitIntentOptions = data['exitIntent'];
	var exitIntentType = exitIntentOptions['option-exit-intent-type'];
	var expireTime = exitIntentOptions['option-exit-intent-expire-time'];
	var exitIntentAlert = exitIntentOptions['option-exit-intent-alert'];
	var exitCookiePageLevel = exitIntentOptions['option-exit-intent-cookie-level'];
	var exitSoftFromTop = exitIntentOptions['option-exit-intent-soft-from-top'];
	this.setExpireTime(expireTime);
	this.setType(exitIntentType);
	this.setExitSoftFromTop(exitSoftFromTop);
	this.setAlertText(exitIntentAlert);
	this.setCookiePageLevel(exitCookiePageLevel);
	this.buildExitIntent(id);
};

