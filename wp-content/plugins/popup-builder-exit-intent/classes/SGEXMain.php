<?php
class SGEXMain {
	
	public function __construct() {

		$this->init();		
	}

	public function init() {

		$this->includePopupConfig();
		$this->actions();
		$this->includeFiles();
	}

	public function includeFiles() {

		require_once(SGPB_EXIT_INTENT_CLASSES."/SGPBExitintentExtension.php");
		require_once(SGPB_EXIT_INTENT_CLASSES."/sgpbExitIntentInstall.php");
		require_once(SGPB_EXIT_INTENT_HELPER."/GetExitintentOptionsData.php");
	}
	
	public function actions() {
		
		register_activation_hook(SGPB_EXIT_INTENT_PATH."/popup-builder-exit-intent.php", array($this,"SGPBExitIntentActivate"));
		register_deactivation_hook(SGPB_EXIT_INTENT_PATH."/popup-builder-exit-intent.php", array($this,"SGPBExitIntentDeactivate"));
		register_uninstall_hook(SGPB_EXIT_INTENT_PATH."/popup-builder-exit-intent.php", array(get_class(),"SGPBExitIntentUninstall"));
	}

	private function includePopupConfig() {

		if(file_exists(WP_PLUGIN_DIR.'/popup-builder-wordpress-plugin')) {
			$pluginFolder = 'popup-builder-wordpress-plugin';
		}
		else if(file_exists(WP_PLUGIN_DIR.'/popup-builder-platinum')) {
			$pluginFolder = 'popup-builder-platinum';
		}
		else if(file_exists(WP_PLUGIN_DIR.'/popup-builder-gold')) {
			$pluginFolder = 'popup-builder-gold';
		}
		else if(file_exists(WP_PLUGIN_DIR.'/popup-builder-silver')) {
			$pluginFolder = 'popup-builder-silver';
		}
		else if(file_exists(WP_PLUGIN_DIR.'/popup-builder')) {
			$pluginFolder = 'popup-builder';
		}
		else {
			$pluginFolder = false;
		}
		if($pluginFolder) {
			$popupConfigPath = WP_PLUGIN_DIR.'/'.$pluginFolder.'/config.php';
			require_once($popupConfigPath);
		}
	}

	public function SGPBExitIntentActivate() {

		sgpbExitIntentInstall::install();
	}

	public function SGPBExitIntentDeactivate() {

		sgpbExitIntentInstall::deactivate();
	}

	public static function SGPBExitIntentUninstall() {

		sgpbExitIntentInstall::uninstall();
	}
}