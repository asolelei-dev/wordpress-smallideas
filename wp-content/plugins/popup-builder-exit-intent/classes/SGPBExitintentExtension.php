<?php
if(!class_exists('SGPBExtension')) {
	if(defined('SG_APP_POPUP_CLASSES')) {
		require_once(SG_APP_POPUP_CLASSES .'/SGPBExtension.php');
	}
	else {
		return;
	}
}
class SGPBExitintentExtension extends SGPBExtension {

	const SGPB_EXTENSION_TYPE = 'option';
	const SGPB_EXTENSION_KEY = 'exitIntent';
	const SGPB_EXTENSION_IS_EVENT = 1;

	public static function addToAddonTable($blogsId) {

		global $wpdb;

		$paths = self::getPaths();
		$type = SGPBExitintentExtension::SGPB_EXTENSION_TYPE;

		$sgInsertCurrentPaths = $wpdb->prepare("INSERT IGNORE ".$wpdb->prefix.$blogsId.SGPBExtension::SGPB_ADDON_TABLE_NAME." (name, paths, type, options, isEvent) VALUES(%s, %s, %s, %s, %d) ", SGPB_EXIT_INTENT_ADDON_KEY, $paths, $type, '', SGPBExitintentExtension::SGPB_EXTENSION_IS_EVENT);

		$wpdb->query($sgInsertCurrentPaths);
	}

	public function getDefaultValues() {

		$defaultParams = array(
			'option-exit-intent-enable' => '',
			'option-exit-intent-type' => "soft",
			'option-exit-intent-expire-time' => '1',
			'option-exit-intent-alert' => '',
			'option-exit-intent-cookie-level' => '1',
			'option-exit-intent-soft-from-top' => ''
		);

		return $defaultParams;
	}

	private function disallowIncludeInPopupTypes() {

		$popupType = array(
			'ageRestriction'
		);

		return $popupType;
	}

	private function extraOptionsKeys() {

		$defaults = $this->getDefaultValues();
		$keys = array_keys($defaults);

		return $keys;
	}

	private static function getPaths() {

		$pathsArray = array();
		$pathsArray['app-path'] = SGPB_EXIT_INTENT_PATH;
		$pathsArray['files-path'] = SGPB_EXIT_INTENT_FILES_PATH;

		return json_encode($pathsArray);
	}

	public function includeOption($popupType) {

		$disallow = $this->disallowIncludeInPopupTypes();
		if(!in_array($popupType, $disallow) && @file_exists(SGPB_EXIT_INTENT_FILES_PATH.'options_section/option.php')) {
			require_once(SGPB_EXIT_INTENT_FILES_PATH.'options_section/option.php');
		}
	}

	public function includeScripts($popupId) {

		wp_register_script('sg_exit_cookie_js', SGPB_EXIT_INTENT_JAVASCRIPT_URL . 'jquery_cookie.js', array(), SGPB_EXIT_INTENT_VERSION);
		wp_enqueue_script('sg_exit_cookie_js');
		
		wp_register_script('sg_exit_intent_js', SGPB_EXIT_INTENT_JAVASCRIPT_URL . 'sg_exit_intent.js', array(), SGPB_EXIT_INTENT_VERSION);
				wp_enqueue_script('sg_exit_intent_js');

		echo "<script>
			sgAddEvent(window, 'load', function() {
				var sgExitIntentObj = new SGExitIntentPopup();
				sgExitIntentObj.init('".$popupId."');
			});</script>";
	}

	private function prepareToSave() {

		$postData = $this->getPostData();
		$extraOptionsKeys = $this->extraOptionsKeys();
		$extensionOptions = array();

		foreach($extraOptionsKeys as $key) {

			if(isset($postData[$key])) {

				$extensionOptions[$key] = $postData[$key];
			}
		}

		$this->setPopupId($postData['popupId']);
		$this->setExtensionContent('');
		$this->setExtensionOptions($extensionOptions);

	}

	public function save() {

		$this->prepareToSave();
		$options = $this->getExtensionOptions();

		if(isset($options['option-exit-intent-enable'])) {
			parent::save();
		}
		else {
			parent::deleteOption();
		}
	}

}
