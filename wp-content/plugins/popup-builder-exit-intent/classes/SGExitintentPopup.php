<?php
require_once(SG_APP_POPUP_CLASSES.'/SGPopup.php');

class SGExitintentPopup extends SGPopup {
	public $content;
	public $exitIntentOptions;

	function __construct()
	{
		wp_register_script('sg_exit_intent_js', SGPB_EXIT_INTENT_JAVASCRIPT_URL . 'sg_exit_intent.js');
		wp_enqueue_script('sg_exit_intent_js');
	}

	public function setContent($content)
	{
		$this->content = $content;
	}

	public function getContent()
	{
		return $this->content;
	}

	public function setExitIntentOptions($options)
	{
		$this->exitIntentOptions = $options;
	}

	public function getExitIntentOptions()
	{
		return $this->exitIntentOptions;
	}

	public static function create($data, $obj = null)
	{
		$obj = new self();

		$options = json_decode($data['options'], true);
		$exitIntentOptions = $options['exitIntentOptions'];

		$obj->setContent($data['exitIntent']);
		$obj->setExitIntentOptions($exitIntentOptions);

		return parent::create($data, $obj);
	}

	public function save($data = array())
	{

		$editMode = $this->getId()?true:false;

		$res = parent::save($data);
		if ($res===false) return false;

		$content = $this->getContent();
		$options = $this->getExitIntentOptions();

		global $wpdb;
		if ($editMode) {
			$content = stripslashes($content);
			$sql = $wpdb->prepare("UPDATE ".$wpdb->prefix."sg_exit_intent_popup SET content=%s, options=%s WHERE id=%d", $content, $options, $this->getId());
			$res = $wpdb->query($sql);
		}
		else {
			$sql = $wpdb->prepare( "INSERT INTO ".$wpdb->prefix."sg_exit_intent_popup (id, content, options) VALUES (%d, %s, %s)", $this->getId(), $content, $options);
			$res = $wpdb->query($sql);
		}
		return $res;
	}

	protected function setCustomOptions($id)
	{
		global $wpdb;
		$st = $wpdb->prepare("SELECT * FROM ". $wpdb->prefix."sg_exit_intent_popup WHERE id = %d", $id);
		$arr = $wpdb->get_row($st, ARRAY_A);
		$this->setContent($arr['content']);
		$this->setExitIntentOptions($arr['options']);
	}

	public function getRemoveOptions()
	{
		return array('onScrolling'=>1,'showOnlyOnce'=>1);
	}

	public function getExitIntentInitScript($id)
	{
		return "<script>
			sgAddEvent(window, 'load', function() {
				sgExitIntentObj = new SGExitIntnetPopup();
				sgExitIntentObj.init($id);
			});</script>";
	}

	/**
	 * Add popup data to footer 
	 *
	 * @since 2.4.3
	 * 
	 * @param string $currentPopupContent popup html content
	 *
	 * @return void
	 *
	 */

	public function addPopupContentToFooter($currentPopupContent) {
	
		echo $currentPopupContent;
	}

	protected function getExtraRenderOptions()
	{

		return array('html'=>'');
	}

	public  function render()
	{
		return parent::render();
	}
}
