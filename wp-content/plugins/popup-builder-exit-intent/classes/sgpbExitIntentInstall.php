<?php
class sgpbExitIntentInstall {

	public static function createTables($blogId = '') {

		SGPBExtension::createExtensionTables($blogId);
		SGPBExitintentExtension::addToAddonTable($blogId);
	}

	public static function install() {

		$obj = new self();
		$obj->createTables();

		/*get_current_blog_id() == 1 When plugin activated inside the child of multisite instance*/
		if(is_multisite() && get_current_blog_id() == 1) {
			global $wp_version;
			if($wp_version > '4.6.0') {
				$sites = get_sites();
			}
			else {
				$sites = wp_get_sites();
			}

			foreach($sites as $site) {

				if($wp_version > '4.6.0') {
					$blogId = $site->blog_id."_";
				}
				else {
					$blogId = $site['blog_id']."_";
				}
				if($blogId != 1) {
					$obj->createTables($blogId);
				}
			}
		}
	}

	public static function uninstallTables($blogId = '') {

	}

	public static function uninstall() {

		$obj = new self();
		$obj->uninstallTables();

		if(is_multisite()) {
			global $wp_version;
			if($wp_version > '4.6.0') {
				$sites = get_sites();
			}
			else {
				$sites = wp_get_sites();
			}
			
			foreach($sites as $site) {
				if($wp_version > '4.6.0') {
					$blogId = $site->blog_id."_";
				}
				else {
					$blogId = $site['blog_id']."_";
				}
				$obj->uninstallTables($blogId);
			}
		}
	}

	public static function deactivatePlugin($blogId = '') {

		global $wpdb;
		
		$deleteFromAddonsSql = "DELETE FROM ".$wpdb->prefix.$blogId."sg_popup_addons WHERE name='".SGPB_EXIT_INTENT_ADDON_KEY."'";
		$wpdb->query($deleteFromAddonsSql);
	}	

	public static function deactivate() {


		self::deactivatePlugin();

		if(is_multisite()) {
			global $wp_version;
			if($wp_version > '4.6.0') {
				$sites = get_sites();
			}
			else {
				$sites = wp_get_sites();
			}
			
			foreach($sites as $site) {
				if($wp_version > '4.6.0') {
					$blogId = $site->blog_id."_";
				}
				else {
					$blogId = $site['blog_id']."_";
				}

				self::deactivatePlugin($blogId);
			}
		}
	}

}