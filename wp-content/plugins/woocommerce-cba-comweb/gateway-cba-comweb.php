<?php
/*
Plugin Name: Commbank Gateway for WooCommerce
Plugin URI: http://tigermodules.net.au
Description: Accept payments using the Commonwealth CommWeb Payment Gateway.
Version: 1.3
Author: TigerModules
Author URI: http://tigermodules.net.au
*/
	require_once( 'woo-includes/woo-functions.php' );

function wc_gateway_cba_plugin_links( $links ) {
	$plugin_links = array(
		'<a href="http://support.tigermodules.net.au/">' . __( 'Support', 'wc_gateway_cba_comweb' ) . '</a>',
	);

	return array_merge( $plugin_links, $links );
}

add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'wc_gateway_cba_plugin_links' );

function wc_gateway_cba_init() {

	if ( ! class_exists( 'WC_Payment_Gateway' ) )
		return;
    load_plugin_textdomain( 'wc_gateway_cba_comweb', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
	class WC_Gateway_CBA_Comweb extends WC_Payment_Gateway {
		public function __construct() {
			global $woocommerce;

			$this->id                 = 'cba_comweb';
			$this->method_title       = __( 'CBA CommWeb', 'wc_gateway_cba_comweb' );
			$this->method_description = __( 'CBA CommWeb offers a complete payment gateway service allowing credit card processing via the internet.', 'wc_gateway_cba_comweb' );
			$this->icon               = WP_PLUGIN_URL . "/" . plugin_basename( dirname( __FILE__ ) ) . '/assets/images/cards.png';
			$this->has_fields		  = true;
			$this->init_form_fields();
			$this->init_settings();
			$this->access_code   = $this->settings['access_code'];
			$this->title         = $this->settings['title'];
			$this->testmode      = $this->settings['testmode'];
			$this->description   = $this->settings['description'];
			$this->merchant      = $this->settings['merchant'];

			if ( $this->testmode == 'yes' && substr( $this->merchant, 0, 4 ) !== 'TEST' )
				$this->merchant = 'TEST' . $this->merchant;

			add_action( 'wp_enqueue_scripts', array( $this, 'payment_scripts' ) );
			add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );

			$this->endpoint    = 'https://migs.mastercard.com.au/vpcdps';
		}

		function payment_scripts() {
			if ( ! is_checkout() )
				return;

			if ( ! wp_script_is( 'wc-credit-card-form', 'registered' ) ) {
				wp_enqueue_style( 'cba-comweb', plugins_url( 'assets/css/checkout.css', __FILE__ ) );
				wp_enqueue_script( 'card-type-detection', plugins_url( 'assets/js/card-type-detection.min.js', __FILE__ ), 'jquery', '1.0.0', true );
			}
		}
	    public function init_form_fields() {
	    	$this->form_fields = array(
				'enabled'         => array(
					'title'       => __( 'Enable/Disable', 'wc_gateway_cba_comweb' ),
					'label'       => __( 'Enable CBA CommWeb', 'wc_gateway_cba_comweb' ),
					'type'        => 'checkbox',
					'description' => '',
					'default'     => 'no'
				),
				'title'           => array(
					'title'       => __( 'Title', 'wc_gateway_cba_comweb' ),
					'type'        => 'text',
					'description' => __( 'This controls the title which the user sees during checkout.', 'wc_gateway_cba_comweb' ),
					'default'     => __( 'Credit card (Commonwealth Bank)', 'wc_gateway_cba_comweb' )
				),
				'description'     => array(
					'title'       => __( 'Description', 'wc_gateway_cba_comweb' ),
					'type'        => 'textarea',
					'description' => __( 'This controls the description which the user sees during checkout.', 'wc_gateway_cba_comweb' ),
					'default'     => __( 'Pay with your credit card via the Commonwealth Bank.', 'wc_gateway_cba_comweb' )
				),
				'testmode'        => array(
					'title'       => __( 'Test Mode', 'wc_gateway_cba_comweb' ),
					'label'       => __( 'Enable Test Mode', 'wc_gateway_cba_comweb' ),
					'type'        => 'checkbox',
					'description' => __( 'Place the payment gateway in development mode.', 'wc_gateway_cba_comweb' ),
					'default'     => 'no'
				),
				'merchant'   => array(
					'title'       => __( 'Merchant ID', 'wc_gateway_cba_comweb' ),
					'type'        => 'text',
					'description' => __( 'This will be provided by the Commonwealth Bank.', 'wc_gateway_cba_comweb' ),
					'default'     => ''
				),
				'access_code' => array(
					'title'       => __( 'Access Code', 'wc_gateway_cba_comweb' ),
					'type'        => 'password',
					'description' => __( 'This will be provided by the Commonwealth Bank.', 'wc_gateway_cba_comweb' ),
					'default'     => ''
				)
			);
		}

		public function is_available() {
			global $woocommerce;

			if ( $this->enabled == "yes" ) {

				if ( ! is_ssl() && $this->testmode == "no" )
					return false;
				if ( ! in_array( get_option( 'woocommerce_currency' ), array( 'AUD', 'NZD' ) ) )
					return false;
				if ( ! $this->access_code || ! $this->merchant )
					return false;

				return true;
			}

			return false;
		}

		public function payment_fields() {
			global $woocommerce;

			if ( $this->testmode == 'yes' )
				$this->description .= ' ' . __( 'TEST MODE/SANDBOX ENABLED', 'wc_gateway_cba_comweb' );

			if ( $this->description )
				echo wpautop( wptexturize( $this->description ) );

			if ( method_exists( $this, 'credit_card_form' ) ) {
				$this->credit_card_form();
			} else {
				?>
				<fieldset class="cba_comweb_form">
					<p class="form-row form-row-wide validate-required cba_comweb_card_number_wrap">
						<label for="cba_comweb_card_number"><?php _e( "Card number", "wc_gateway_cba_comweb" ) ?></label>
						<input type="text" class="input-text" name="cba_comweb-card-number" id="cba_comweb_card_number" pattern="[0-9]{12,19}" />
						<span id="cba_comweb_card_type_image"></span>
					</p>
					<p class="form-row form-row-first validate-required">
						<label for="cba_comweb_card_expiration"><?php _e( "Expiry date <small>(MMYY)</small>", "wc_gateway_cba_comweb" ) ?></label>
						<input type="text" class="input-text" placeholder="MMYY" name="cba_comweb-card-expiry" id="cba_comweb_card_expiration" size="4" maxlength="4" max="1299" min="0100" pattern="[0-9]+" />
					</p>
					<p class="form-row form-row-last validate-required">
						<label for="cba_comweb_card_csc"><?php _e( "Card security code", "wc_gateway_cba_comweb" ) ?></label>
						<input type="text" class="input-text" id="cba_comweb_card_csc" name="cba_comweb-card-cvc" maxlength="4" size="4" pattern="[0-9]+" />
					</p>
					<div class="clear"></div>
				</fieldset>
				<?php

				$woocommerce->add_inline_js( "
					jQuery('form.checkout, #order_review').on( 'keyup change blur', '#cba_comweb_card_number', function() {
						var csc = jQuery('#cba_comweb_card_csc').parent();
						var card_number = jQuery('#cba_comweb_card_number').val();

						jQuery('#cba_comweb_card_type_image').attr('class', '');

						if ( is_valid_card( card_number ) ) {

							var card_type = get_card_type( card_number );

							if ( card_type ) {
								jQuery('#cba_comweb_card_type_image').addClass( card_type );

								if ( card_type == 'visa' || card_type == 'amex' || card_type == 'discover' || card_type == 'mastercard' ) {
									csc.show();
								} else {
									csc.hide();
								}
							}

							jQuery('#cba_comweb_card_number').parent().addClass('woocommerce-validated').removeClass('woocommerce-invalid');
						} else {
							jQuery('#cba_comweb_card_number').parent().removeClass('woocommerce-validated').addClass('woocommerce-invalid');
						}
					}).change();
				" );
			}
		}

		public function process_payment( $order_id ) {
			global $woocommerce;

			$order   = new WC_Order( $order_id );

			$request = new stdClass();

			$card_number = ! empty( $_POST['cba_comweb-card-number'] ) ? str_replace( array( ' ', '-' ), '', woocommerce_clean( $_POST['cba_comweb-card-number'] ) ) : '';
			$card_csc    = ! empty($_POST['cba_comweb-card-cvc'] ) ? woocommerce_clean( $_POST['cba_comweb-card-cvc'] ) : '';
			$card_expiry = ! empty( $_POST['cba_comweb-card-expiry'] ) ? woocommerce_clean( $_POST['cba_comweb-card-expiry'] ) : '';

			$card_expiry    = implode( '', array_map( 'trim', explode( '/', $card_expiry ) ) );
			$card_exp_year  = substr( $card_expiry, 2, 2 );
			$card_exp_month = substr( $card_expiry, 0, 2 );

			$request->vpc_Version          = 1;
			$request->vpc_Command          = 'pay';
			$request->vpc_MerchTxnRef      = $order_id . '/' . md5( microtime() );
			$request->vpc_AccessCode       = $this->access_code;
			$request->vpc_Merchant         = $this->merchant;
			$request->vpc_OrderInfo        = preg_replace('/[^\da-z]/i', '', $order->get_order_number() );
			$request->vpc_Amount           = $order->get_total() * 100;
			$request->vpc_CardNum          = $card_number;
			$request->vpc_CardExp          = $card_exp_year . $card_exp_month;
			$request->vpc_CardSecurityCode = $card_csc;

			try {
				if ( empty( $card_number ) ) {
					throw new Exception( __( 'Please enter your card number.', 'wc_gateway_cba_comweb' ) );
				}

				$response = wp_remote_post( $this->endpoint, array(
				    'method'    => 'POST',
				    'body'      => http_build_query( $request ),
				    'timeout'   => 70,
				    'sslverify' => true
				) );

				if ( is_wp_error( $response ) )
					throw new Exception( __( 'There was a problem connecting to the payment gateway.', 'wc_gateway_cba_comweb' ) );

				if ( empty( $response['body'] ) )
					throw new Exception( __( 'Empty response.', 'wc_gateway_cba_comweb' ) );

				parse_str( $response['body'], $parsed_response );

				$response_code = $parsed_response['vpc_TxnResponseCode'];

				switch ( $response_code ) {
					case "0" :
						$order->add_order_note( sprintf( __( 'Commonwealth Bank payment completed (Transaction # %s)', 'wc_gateway_cba_comweb' ), $parsed_response['vpc_TransactionNo'] ) );
						$order->payment_complete();
						$woocommerce->cart->empty_cart();

						return array(
							'result' 	=> 'success',
							'redirect'	=> $this->get_return_url( $order )
						);
					break;
					default :
						$order->update_status( 'failed', sprintf( __('Commonwealth Bank payment failed. Payment was rejected due to an error: %s', 'wc_gateway_cba_comweb' ), $parsed_response['vpc_Message'] ) );

						//$woocommerce->add_error( sprintf( __( 'Payment failed: %s', 'wc_gateway_cba_comweb' ), $parsed_response['vpc_Message'] ) );
						//$woocommerce->add_notice( sprintf( __( 'Payment failed: %s', 'wc_gateway_cba_comweb' ), $parsed_response['vpc_Message'] ) );
						wc_print_notice( __( 'Payment failed: '.$parsed_response['vpc_Message'], 'woocommerce' ), 'error' );
						return;
					break;
				}
			} catch ( Exception $e ) {
				//$woocommerce->add_error( sprintf( __( 'Error: %s', 'wc_gateway_cba_comweb' ), $e->getMessage() ) );
//				$woocommerce->add_notice( sprintf( __( 'Error: %s', 'wc_gateway_cba_comweb' ), $e->getMessage() ) );
				wc_print_notice( __( 'Error: '.$e->getMessage(), 'woocommerce' ), 'error' );
				return;
			}
		}
	}

	function wc_gateway_cba_add( $methods ) {
		$methods[] = 'WC_Gateway_CBA_comweb';
		return $methods;
	}

	add_filter( 'woocommerce_payment_gateways', 'wc_gateway_cba_add' );
}

add_action( 'plugins_loaded', 'wc_gateway_cba_init', 0 );